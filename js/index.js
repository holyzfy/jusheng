(function () {

function toggleIsland(index) {
    $('#island-list .island-' + index).siblings('.selected').removeClass('selected').end().toggleClass('selected');
    var $current = $('#svg .polygon-' + index);
    $current.appendTo('#svg').siblings('.selected').removeClass('selected').end().toggleClass('selected');
    !$current.hasClass('selected') && setTimeout(function () {
        $('#svg').html($('#svg').data('html'));
    }, 500);
}

$('#svg').on('click', 'polygon', function () {
    toggleIsland($(this).data('island'));
});

$('#nav-hd').click(function () {
    $('#nav').toggleClass('selected');
});

$('#nav li').hover(function () {
    var index = $(this).data('island');
    $('#island-list .island-' + index).addClass('selected');
}, function () {
    $('#island-list .selected').removeClass('selected');
});

function initPopup() {
    $('#nav').on('click', 'li', function () {
        var index = $(this).data('island') - 1;
        showPopup(index);
    });

    $('#island-list').on('click', 'span', function () {
        var index = $(this).data('island') - 1;
        showPopup(index);
        $('#island-list .selected').removeClass('selected');
    });

    $('#popup').on('click', 'a.popup-close, .popup-overlay', function (event) {
        event.preventDefault();
        $('#popup').empty();
    });
}

function showPopup(index) {
    showPopup.render = showPopup.render || template('popup-template');
    var html = showPopup.render(data[index]);
    $('#popup').html(html);
    slide();
}

function slide() {
    $('#popup-bd').slick({
        respondTo: 'slider',
        infinite: false
    });
}

function loading() {
    $('#wrap').waitForImages({
        finished: function() {
            $('#loading').fadeOut('slow', function () {
                $(this).remove();
            });
        },
        each: function(loaded, count, success) {
           var percentage = parseInt((loaded + 1) * 100 / count) + '%';
           $('#loading-percentage').css('width', percentage);
        },
        waitForAll: true
    });

    setTimeout(function () {
        $('#loading').addClass('loading-toggle');
    }, 1000);
}

function init() {
    initPopup();
    $('#svg').data('html', $('#svg').html());
    loading();
}

init();

})();