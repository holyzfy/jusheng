var data = [
    {
        images: [
            'data/1/1.jpg',
            'data/1/2.jpg'
        ],
        info: [
            {
                title: '司令部',
                description: '建筑面积6700平方米，主体建筑16栋，区域全面，功能独立分明。'
            },
            {
                title: '主要区域',
                description: '办公楼、军校、兵营、战俘营、小广场......'
            }
        ]
    },
    {
        images: [
            'data/2/1.jpg',
            'data/2/2.jpg'
        ],
        info: [
            {
                title: '景区接待',
                description: '有氧办公、绿色低碳\n景区接待区根据现状条件引入水体景观，吐过水为媒介，创意性修建了亲水平台，在合理的改善接待区环境的同时，也给到访的游客及景区管理者提供一个舒适的游览环境和工作氛围。'
            }
        ]
    },
    {
        images: [
            'data/3/1.jpg',
            'data/3/2.jpg',
            'data/3/2.jpg'
        ],
        info: [
            {
                title: '景观观赏区',
                description: '景观占地面积约为46705.5平方米。'
            }
        ]
    },
    {
        images: [
            'data/4/1.jpg',
            'data/4/2.jpg'
        ],
        info: [
            {
                title: '酒店区',
                description: '年代与现代的集合，酒店共有18栋，561间客房（包含四星酒店楼层1栋104套房间，采用美国路创酒店智能管理系统）'
            }
        ]
    },
    {
        images: [
            'data/5/1.jpg',
            'data/5/2.jpg'
        ],
        info: [
            {
                title: '誓言广场，信仰之力',
                description: ''
            }
        ]
    },
    {
        images: [
            'data/6/1.jpg',
            'data/6/2.jpg'
        ],
        info: [
            {
                title: '火车站、摄影区',
                description: '火车站：改建建筑12栋，改建建筑面积为11364.76平方米。'
            }
        ]
    },
    {
        images: [
            'data/7/1.jpg',
            'data/7/2.jpg'
        ],
        info: [
            {
                title: '古镇游览区',
                description: '古镇：建改建筑25栋，改建建筑面积为10392.04平方米。'
            }
        ]
    },
    {
        images: [
            'data/8/1.jpg',
            'data/8/2.jpg'
        ],
        info: [
            {
                title: '宅院区，民国文化',
                description: '宅院：新建建筑面积为17010.23平方米'
            }
        ]
    },
    {
        images: [
            'data/9/1.jpg',
            'data/9/2.jpg'
        ],
        info: [
            {
                title: '特色村落区，地域民俗',
                description: '村落：新建建筑30栋。新建建筑面积为3805.86平方米'
            }
        ]
    },
    {
        images: [
            'data/10/1.jpg',
            'data/10/2.jpg'
        ],
        info: [
            {
                title: '机场区，复原历史',
                description: '机场：新建建筑9栋，改建建筑3栋。新建建筑面积为9957.06平方米。改建建筑面积为2775.2平方米'
            }
        ]
    },
    {
        images: [
            'data/11/1.jpg',
            'data/11/2.jpg'
        ],
        info: [
            {
                title: '从林野战区，战场硝烟',
                description: ''
            }
        ]
    },
    {
        images: [
            'data/12/1.jpg',
            'data/12/2.jpg'
        ],
        info: [
            {
                title: '原始森林区',
                description: ''
            }
        ]
    },
    {
        images: [
            'data/13/1.jpg',
            'data/13/2.jpg'
        ],
        info: [
            {
                title: '苗寨，民族风情',
                description: ''
            }
        ]
    },
    {
        images: [
            'data/14/1.jpg',
            'data/14/2.jpg'
        ],
        info: [
            {
                title: '悬崖酒店，低调奢华',
                description: '全部采用美国路创智能酒店管理系统'
            }
        ]
    }
];